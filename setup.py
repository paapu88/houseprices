"""
pip install -e .
"""

import setuptools

with open("./README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="houseprice",
    version="2023.8",
    author="Markus",
    author_email="markus.kaukonen@iki.fi",
    description="house prices",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Operating System :: POSIX :: Linux",
    ],
    python_requires=">=3.8",
    install_requires=[
        "pandas>=pandas-2.0.3",
        "scipy>=1.11.1",
        "scikit-learn>=1.3.0",
        "omegaconf>=2.3.0",
    ],
)
