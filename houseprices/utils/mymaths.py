import pandas as pd
from typing import List
import numpy as np


def to_sincos(df: pd.DataFrame, sincosCols: List[str]) -> (pd.DataFrame, List[str]):
    """for all dataframe columns listed in sincosCols,
        make each column double using 0..1 scaled variable and sin and cos on it.
        In this way there will be no discontinuity in periodic variables.

    Args:
        df (pd.DataFrame):      original dataframe
        sincosCols (List[str]): list of columns to be modified (doubled)

    Returns:
        pd.DataFrame:   modified dataframe with new columns,
                        their labels are X_sin and X_cos instead of the original X
        List[str]:      List of new column names
    """
    # X to sinX & cosX
    new_cols = []
    for sincosCol in sincosCols:
        mymin = df[sincosCol].min()
        mymax = df[sincosCol].max()
        newsinCol = sincosCol + "_sin"
        newcosCol = sincosCol + "_cos"
        df[newsinCol] = np.sin(2 * np.pi * (df[sincosCol] - mymin) / mymax)
        df[newcosCol] = np.cos(2 * np.pi * (df[sincosCol] - mymin) / mymax)
        new_cols.append(newsinCol)
        new_cols.append(newcosCol)
    return df.drop(sincosCols, axis=1), new_cols
