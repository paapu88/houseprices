from sklearn.impute import SimpleImputer
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.pipeline import Pipeline


def mlp_pipelines():
    """define data pipeline for multilayer perceptors,
        for three different datatypes (numerical, ordered categories, and one hot encoded categories)

    Returns:
        three pipelines, one for each datatype (numerical, ordered categories, and one hot encoded categories)
    """
    # Preprocessing for numerical data
    numerical_transformer = Pipeline(
        steps=[
            (
                "imputer",
                SimpleImputer(strategy="median"),
            ),  # Fill missing values with the median
            (
                "scaler",
                StandardScaler(),
            ),  # Standardize features by removing the mean and scaling to unit variance
        ]
    )
    # Preprocessing for ordered categories
    quality_transformer = Pipeline(
        steps=[
            (
                "imputer",
                SimpleImputer(strategy="median"),
            ),  # Fill missing values with the median
            (
                "scaler",
                MinMaxScaler(),
            ),  # Standardize features by minmax, because this is a quality scale
        ]
    )

    # Preprocessing for one hot encoded categorical data
    categorical_transformer = Pipeline(
        steps=[
            (
                "imputer",
                SimpleImputer(strategy="most_frequent"),
            ),  # Fill missing values with the most frequent value
        ]
    )
    return numerical_transformer, quality_transformer, categorical_transformer


def tree_pipelines():
    """define data pipeline for tree based regressors,
        for three different datatypes (numerical, ordered categories, and one hot encoded categories)

    Returns:
        three pipelines, one for each datatype (numerical, ordered categories, and categories)
    """
    # Preprocessing for numerical data
    numerical_transformer = Pipeline(
        steps=[
            (
                "imputer",
                SimpleImputer(strategy="median"),
            ),  # Fill missing values with the median
        ]
    )
    # Preprocessing for ordered categories
    quality_transformer = Pipeline(
        steps=[
            (
                "imputer",
                SimpleImputer(strategy="median"),
            ),  # Fill missing values with the median
        ]
    )

    # Preprocessing for categorical data
    categorical_transformer = Pipeline(
        steps=[
            (
                "imputer",
                SimpleImputer(strategy="most_frequent"),
            ),  # Fill missing values with the most frequent value
        ]
    )
    return numerical_transformer, quality_transformer, categorical_transformer
