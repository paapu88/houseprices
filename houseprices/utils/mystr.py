import numpy as np
from typing import Union


def convert_NA(val: str) -> Union[str, np.ndarray]:
    """convert all NA related strings to np.nan

    Args:
        val: a string that might be NA

    Returns:
        np.nan if NA is in string, otherwise the original string
    """
    if isinstance(val, str):
        val = val.strip()  # Remove leading/trailing whitespaces
        if val == "NA":
            return np.nan
    return val
