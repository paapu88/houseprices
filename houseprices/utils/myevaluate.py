from sklearn.metrics import (
    mean_squared_error,
    mean_absolute_error,
    r2_score,
)


def evaluate(model, X_test, y_test):
    """evaluate model using X_test as data and y_test as ground truth

    Args:
        model (_type_): a scikit-learn model
        X_test (_type_): normalised test data
        y_test (_type_): target
    """
    y_pred = model.predict(X_test)
    mse = mean_squared_error(y_test, y_pred, squared=False)
    mae = mean_absolute_error(y_test, y_pred)
    r2 = r2_score(y_test, y_pred)
    print(f"MSE: {mse}\nMAE: {mae}\nR2 score: {r2}")
