import pandas as pd
from sklearn.preprocessing import OrdinalEncoder
from omegaconf import OmegaConf
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import LabelEncoder
from typing import List


def make_ordinal_encoding(
    df: pd.DataFrame, cfg: OmegaConf, strategy: str = "most_frequent"
) -> pd.DataFrame:
    """for categorical variables with order, make them have order with OrdinalEncoder

    Args:
        df (pd.DataFrame):  Original dataframe
        cfg (OmegaConf):    conf data containing labels of columns that are categorical-ordinal and their order.
        strategy(str):      imputing strategy for sklearn
    Returns:
        pd.DataFrame:  dataframe with ordinal features encoded to whole float numbers
    """
    for col in cfg.quality_cols:
        print(f"col: {col}")
        ordering = [cfg.ordinal_categories[col]]
        # impute most frequent value instead of nan
        # Impute NaN with the most frequent value in the column
        imputer = SimpleImputer(strategy=strategy)
        imputed_data = imputer.fit_transform(df[[col]])
        # Initialize and fit the encoder
        encoder = OrdinalEncoder(categories=ordering)
        df[col] = encoder.fit_transform(imputed_data)
    # df = df.drop(cfg.quality_cols, axis=1)
    return df


def make_category_encoding(
    df: pd.DataFrame, categorical_cols: List[str], strategy: str = "most_frequent"
) -> pd.DataFrame:
    """for categorical variables with NO order, just convert categories to random floats

    Args:
        df (pd.DataFrame):  Original dataframe
        categorical_cols:   a list of categorical column names
        strategy(str):      imputing strategy for sklearn
    Returns:
        pd.DataFrame:  dataframe with ordinal features encoded to whole float numbers
    """
    le = LabelEncoder()

    for col in categorical_cols:
        print(f"categorical col: {col}")

        # impute most frequent value instead of nan
        # Impute NaN with the most frequent value in the column
        imputer = SimpleImputer(strategy=strategy)
        imputed_data = imputer.fit_transform(df[[col]])
        # Initialize and fit the encoder
        df[col] = le.fit_transform(imputed_data)
    return df
