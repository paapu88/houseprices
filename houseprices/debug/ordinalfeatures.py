import pandas as pd
from sklearn.preprocessing import OrdinalEncoder

# Sample data
data = {"Size": ["Small", "Medium", "Large", "Small", "Large", "Medium"]}
df = pd.DataFrame(data)

# Define ordering for the 'Size' feature
ordering = [["Small", "Medium", "Large"]]

# Initialize and fit the encoder
encoder = OrdinalEncoder(categories=ordering)
df["Size_encoded"] = encoder.fit_transform(df[["Size"]])

print(df)
