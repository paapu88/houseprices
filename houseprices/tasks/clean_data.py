"""
# TRAIN: prepare for random forest and other trees
python clean_data.py \
    --features_file ../../confs/houseprice_features.yaml \
    --instructions_file ../../confs/houseprice_train_forests.yaml

# TRAIN: prepare for multi layer perceptors
python clean_data.py \
    --features_file ../../confs/houseprice_features.yaml \
    --instructions_file ../../confs/houseprice_train_mlp.yaml

# TEST: prepare for random forest and other trees
python clean_data.py \
    --features_file ../../confs/houseprice_features.yaml \
    --instructions_file ../../confs/houseprice_test_forests.yaml

# TEST: prepare for multi layer perceptors
python clean_data.py \
    --features_file ../../confs/houseprice_features.yaml \
    --instructions_file ../../confs/houseprice_test_mlp.yaml

"""


import pandas as pd
import numpy as np
from scipy import stats

import argparse
from houseprices.utils.mymaths import to_sincos
from houseprices.utils.myfeatures import make_ordinal_encoding, make_category_encoding
from houseprices.utils.mypipelines import mlp_pipelines, tree_pipelines
from omegaconf import OmegaConf
from sklearn.compose import ColumnTransformer

parser = argparse.ArgumentParser(
    description="argparse only takes the name of the conf file, which then containins all parameters"
)
parser.add_argument(
    "--features_file",
    type=str,
    help="yaml configuration file for features",
)
parser.add_argument(
    "--instructions_file",
    type=str,
    help="yaml configuration for data processing",
)
args = parser.parse_args()

# read feature constants
with open(args.features_file) as f:
    cfg_features = OmegaConf.load(f)
# read general constants
with open(args.instructions_file) as f:
    cfg_instructions = OmegaConf.load(f)

print(f"conf FEATURES for feature engineering:")
print(cfg_features)
print(f"conf INSTRUCTIONS for data processing:")
print(cfg_instructions)

# Load data
df = pd.read_csv(
    cfg_instructions.csvfile,
    # converters={col: convert_NA for col in args.problemCols},
)
df = df.drop(cfg_features.dropCols, axis=1)
print(df.head())
print(df.info())

# POSSIBLY WRITE TARGET Y to csv file
print(f"cfg_instructions.label: {cfg_instructions.label}")
if cfg_instructions.label is not None:
    # we have target column in csv and write it untreated to csv
    # check target column
    print(
        f"in target column, number of nans: {df[cfg_instructions.label].isna().sum()}"
    )
    z_scores = stats.zscore(df[cfg_instructions.label])
    outliers = np.where(np.abs(z_scores) > 3)
    print(f"in target column, outlier indexes: {outliers}")

    # Write untreated target column to CSV file
    df[cfg_instructions.label].to_csv(
        str(cfg_instructions.csvfile).replace(".csv", "_unprocessed_y.csv"), index=False
    )

    # Then delete target column from the DataFrame
    df = df.drop(cfg_instructions.label, axis=1)

print(df.describe())

# make months periodic
df, newsincosCols = to_sincos(df=df, sincosCols=cfg_features.sincosCols)
# convert numerical columns to float

df[list(cfg_features.floatCols)] = df[list(cfg_features.floatCols)].astype(float)
df[newsincosCols] = df[newsincosCols].astype(float)

# convert quality_cols to ridnal cols (floats of integer values)
df = make_ordinal_encoding(df=df, cfg=cfg_features)

# convert categorical variables to objects
df[list(cfg_features.categorical_cols)] = df[
    list(cfg_features.categorical_cols)
].astype(str)

print(df.head())
print(df.info())

# Handle outliers - we'll use Z-score to detect and handle outliers
# ok this was a bad idea because outliers seem to be true
# for col in args.floatCols:
#    z_scores = stats.zscore(df[col])
#    abs_z_scores = np.abs(z_scores)
#    outlier_rows = abs_z_scores > 3
#    df.loc[outlier_rows, col] = np.nan

# CATEGIRICAL FEATURES TO ONE HOT IF MLP-/LINEARREGRESSOR
if cfg_instructions.scaledOneHot:
    # to one hot for mlp
    print(f"converting categories to one hot")
    df = pd.get_dummies(df, columns=cfg_features.categorical_cols, dtype=bool)
    # Convert all boolean columns to int
    categorical_cols = df.select_dtypes(include=["bool"]).columns
    df[categorical_cols] = df[categorical_cols].astype(float)
    df.to_csv("onehot.csv")
else:
    # for trees, encode categorical features to numbers, order has no meaning
    categorical_cols = list(cfg_features.categorical_cols)
    df = make_category_encoding(df=df, categorical_cols=categorical_cols)
print(df.info())

# DATA PROCESSING PIPELINES
if cfg_instructions.scaledOneHot:
    (
        numerical_transformer,
        quality_transformer,
        categorical_transformer,
    ) = mlp_pipelines()
else:
    (
        numerical_transformer,
        quality_transformer,
        categorical_transformer,
    ) = tree_pipelines()

# Bundle preprocessing for numerical, ordered-categorical and categorical data
preprocessor = ColumnTransformer(
    transformers=[
        ("num", numerical_transformer, list(cfg_features.floatCols) + newsincosCols),
        ("qua", quality_transformer, list(cfg_features.quality_cols)),
        ("cat", categorical_transformer, list(categorical_cols)),
    ]
)

print(df.head())
print(df.describe())
print(df.info())
df.to_csv("beforeX.csv")
# Preprocess data
X = preprocessor.fit_transform(df)
# to make more similar to standard scaler

print(f"len(numerical_cols) {len(list(cfg_features.floatCols) + newsincosCols)}")
print(f"len(quality_cols) {len(list(cfg_features.quality_cols))}")
print(f"len(categorical_cols) {len(categorical_cols)}")

print(f"X: {X.shape}")
print(f"X {X[0,0:10]}")


# Convert processed data back to dataframe
if cfg_instructions.scaledOneHot:
    X_df = pd.DataFrame(X, columns=df.columns)
else:
    assert X.shape[1] == len(list(cfg_features.floatCols) + newsincosCols) + len(
        list(cfg_features.quality_cols)
    ) + len(categorical_cols), "Check your features, sums do not match!"

    X_df = pd.DataFrame(
        X,
        columns=list(cfg_features.floatCols)
        + newsincosCols
        + list(cfg_features.quality_cols)
        + categorical_cols,
    )

# add -0.5 to resemble standard scaling for mlps
if cfg_instructions.scaledOneHot:
    X_df[list(cfg_features.quality_cols)] = X_df[list(cfg_features.quality_cols)] - 0.5
    X_df[categorical_cols] = X_df[categorical_cols] - 0.5
# Write dataframes to csv
print(X_df.head())

X_df.to_csv(
    str(cfg_instructions.csvfile).replace(".csv", cfg_instructions.postfix), index=False
)
