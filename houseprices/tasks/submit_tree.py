import pandas as pd
from sklearn.ensemble import (
    GradientBoostingRegressor,
)
from sklearn.model_selection import train_test_split
from houseprices.utils.myevaluate import evaluate
import numpy as np


# Load train data
X = pd.read_csv("../../data/train_processed_X_for_tree.csv")
y = np.ravel(pd.read_csv("../../data/train_unprocessed_y.csv"))
model = GradientBoostingRegressor(
    random_state=1502, min_samples_split=3, n_estimators=300, subsample=1.0
)
# evaluate is not important here, because we have already selected paramters andwill use competition test set
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.01, random_state=1502
)
# train and evaluate model
model.fit(X_train, y_train)
evaluate(model=model, X_test=X_test, y_test=y_test)

# load prepared test data
X = pd.read_csv("../../data/test_processed_X_for_tree.csv")

# predict
prediction = model.predict(X)

# prepare submission csv
sample_submission_df = pd.read_csv("../../data/sample_submission.csv")
sample_submission_df["SalePrice"] = prediction
sample_submission_df.to_csv("../../data/submission.csv", index=False)
print(sample_submission_df.head())
