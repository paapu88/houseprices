import pandas as pd
from typing import List
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import (
    RandomForestRegressor,
    HistGradientBoostingRegressor,
    GradientBoostingRegressor,
)
from sklearn.neural_network import MLPRegressor
from sklearn.metrics import (
    mean_squared_error,
    mean_absolute_error,
    r2_score,
    make_scorer,
)
from sklearn.pipeline import Pipeline
from sklearn.decomposition import PCA
import numpy as np


class ModelTrainer:
    def __init__(
        self,
        X_train: pd.DataFrame,
        X_test: pd.DataFrame,
        y_train: pd.Series,
        y_test: pd.Series,
        feature_names: List[str],
    ):
        self.X_train = X_train
        self.X_test = X_test
        self.y_train = y_train
        self.y_test = y_test
        self.feature_names = feature_names
        self.model = None

    def train(self):
        raise NotImplementedError("Subclass must implement abstract method")

    def evaluate(self):
        y_pred = self.model.predict(self.X_test)
        mse = mean_squared_error(self.y_test, y_pred, squared=False)
        mae = mean_absolute_error(self.y_test, y_pred)
        r2 = r2_score(self.y_test, y_pred)
        print(f"MSE: {mse}\nMAE: {mae}\nR2 score: {r2}")

    def feature_importance(self):
        if hasattr(self.model, "feature_importances_"):
            importances = self.model.feature_importances_
        else:
            print("The model doesn't provide feature importances")
            return

        # Get the indices of the top 10 features
        indices = np.argsort(importances)[-10:]
        # Print the feature names and their importance values
        for i in reversed(indices):
            print(f"{self.feature_names[i]}: {importances[i]}")


class LinearModelTrainer(ModelTrainer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = LinearRegression()

    def train(self):
        param_grid = {
            "pca__n_components": [None, 50, 100, 200],  # None will retain all features
        }
        # Setting up the scorer for GridSearchCV
        scorer = make_scorer(mean_squared_error, greater_is_better=False)
        pipeline = Pipeline([("pca", PCA()), ("linear", self.model)])
        # Initializing GridSearch with the model and parameter grid
        grid_search = GridSearchCV(
            estimator=pipeline,
            param_grid=param_grid,
            scoring=scorer,
            cv=5,
            verbose=1,
            n_jobs=-1,
        )

        grid_search.fit(self.X_train, self.y_train)
        self.model = grid_search.best_estimator_
        print("Best Parameters:", grid_search.best_params_)


class MLPModelTrainer(ModelTrainer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = MLPRegressor(max_iter=1000, random_state=1502)

    def train(self):
        pipeline = Pipeline([("mlp", self.model)])  # ("pca", PCA()),
        param_grid = {
            # "pca__n_components": [None, 50, 100, 200],  # None will retain all features
            "mlp__hidden_layer_sizes": [
                (a, b, c)
                for a in [10, 100, 200]
                for b in [10, 100, 200]
                for c in [10, 100, 200]
            ],
        }
        # Setting up the scorer for GridSearchCV
        scorer = make_scorer(mean_squared_error, greater_is_better=False)

        # Initializing GridSearch with the model and parameter grid
        grid_search = GridSearchCV(
            estimator=pipeline,
            param_grid=param_grid,
            scoring=scorer,
            cv=5,
            verbose=1,
            n_jobs=-1,
        )

        grid_search.fit(self.X_train, self.y_train)
        self.model = grid_search.best_estimator_
        print("Best Parameters:", grid_search.best_params_)


class RFModelTrainer(ModelTrainer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = RandomForestRegressor(random_state=1502)

    def train(self):
        param_grid = {
            "n_estimators": [10, 50, 100],
            "max_depth": [
                None,
                10,
            ],
            "min_samples_split": [2, 5, 10],
        }
        # Setting up the scorer for GridSearchCV
        scorer = make_scorer(mean_squared_error, greater_is_better=False)

        # Initializing GridSearch with the model and parameter grid
        grid_search = GridSearchCV(
            estimator=self.model,
            param_grid=param_grid,
            scoring=scorer,
            cv=5,
            verbose=1,
            n_jobs=-1,
        )

        grid_search.fit(self.X_train, self.y_train)
        self.model = grid_search.best_estimator_
        print("Best Parameters:", grid_search.best_params_)


class HGBModelTrainer(ModelTrainer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = HistGradientBoostingRegressor(random_state=1502)

    def train(self):
        param_grid = {
            "max_depth": [
                None,
                10,
            ],
            "max_leaf_nodes": [
                None,
                5,
                11,
                21,
            ],
            "min_samples_leaf": [10, 20, 30],
        }
        # Setting up the scorer for GridSearchCV
        scorer = make_scorer(mean_squared_error, greater_is_better=False)

        # Initializing GridSearch with the model and parameter grid
        grid_search = GridSearchCV(
            estimator=self.model,
            param_grid=param_grid,
            scoring=scorer,
            cv=5,
            verbose=1,
            n_jobs=-1,
        )

        grid_search.fit(self.X_train, self.y_train)
        self.model = grid_search.best_estimator_
        print("Best Parameters:", grid_search.best_params_)


class GBModelTrainer(ModelTrainer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = GradientBoostingRegressor(random_state=1502)

    def train(self):
        param_grid = {
            "n_estimators": [200, 300, 400],
            "subsample": [0.8, 0.9, 1.0],
            "min_samples_split": [2, 3],
        }
        # Setting up the scorer for GridSearchCV
        scorer = make_scorer(mean_squared_error, greater_is_better=False)

        # Initializing GridSearch with the model and parameter grid
        grid_search = GridSearchCV(
            estimator=self.model,
            param_grid=param_grid,
            scoring=scorer,
            cv=5,
            verbose=1,
            n_jobs=-1,
        )

        grid_search.fit(self.X_train, self.y_train)
        self.model = grid_search.best_estimator_
        print("Best Parameters:", grid_search.best_params_)


def main():
    models = {
        "linear": LinearModelTrainer,
        "rfm": RFModelTrainer,
        "hgb": HGBModelTrainer,
        "gbm": GBModelTrainer,
        "mlp": MLPModelTrainer,
    }
    y = np.ravel(pd.read_csv("../../data/train_unprocessed_y.csv"))

    for name, model in models.items():
        print(f"model {model}")
        if name in ["linear", "mlp"]:
            print(f"reading csv for linear/mlp!")
            X = pd.read_csv("../../data/train_processed_X_for_mlp.csv")
        else:
            print(f"reading csv for tree!")
            X = pd.read_csv("../../data/train_processed_X_for_tree.csv")

        X_train, X_test, y_train, y_test = train_test_split(
            X, y, test_size=0.1, random_state=1502
        )

        feature_names = X.columns.tolist()
        print(f"feature names: {feature_names}")

        trainer = model(X_train, X_test, y_train, y_test, feature_names)
        trainer.train()
        trainer.evaluate()
        trainer.feature_importance()
        print(f"{80*'*'}")


if __name__ == "__main__":
    main()
