# house prices

- background: see example notebook (referred here as KaggleSample) to get familiar with the data https://www.kaggle.com/code/gusthema/house-prices-prediction-using-tfdf/notebook

- results from demo notebook above are for the val set (in dollars):
```
mean_absolute_error: 15953
root mean_squared_error: 26186
```

- google search, stack overflow and chatgpt 4.0 are used.

- here we only study a small fraction of possible parameters and possible regressor methods.

# install (linux only, tested ubuntu 22.04)

- clone repo

```
mkdir ~/git
cd ~/git
git clone https://gitlab.com/paapu88/houseprices.git
cd houseprices
```

- setup virtual environment and install houseprices as a package

```
mkdir ~/venvs/
python -m venv ~/venvs/houseprices
source ~/venvs/houseprices/bin/activate

cd ~/git/houseprices
pip install -e .

```
# clean data

- usage for preparation for the training set:
```
cd ~/git/houseprices/houseprices/tasks
# prepare for random forest and other trees:
python clean_data.py \
    --features_file ../../confs/houseprice_features.yaml \
    --instructions_file ../../confs/houseprice_train_forests.yaml

# prepare for multi layer perceptors and linear fit:
python clean_data.py \
    --features_file ../../confs/houseprice_features.yaml \
    --instructions_file ../../confs/houseprice_train_mlp.yaml

```

- resulting cleaned and normalised data for training:

```
~/git/houseprices/data/train_unprocessed_y.csv
~/git/houseprices/data/train_processed_X_for_tree.csv
~/git/houseprices/data/train_processed_X_for_mlp.csv
```



# search for best model with dimension reduction and parameter optimisation

- usage:
```
cd ~/git/houseprices/houseprices/tasks
python grid_train.py
```

- results (from output of grid_train.py):

| Method   |  MAE     | RMSE     | R2       | best parameters |
|----------|----------|----------|----------|-----------------|
| KaggleSample           | 15953       | 26186       | -       | -       |
| LinearRegression       | 17069       | 22489       | 0.88       | pca__n_components: 100       |
| RandomForestRegressor  | 16055       | 21335       | 0.90       | max_depth:10, min_samples_split: 10, n_estimators:100 |
| HistGradientBoostingRegressor | 14756       |   19862  | 0.91 | max_depth: None, max_leaf_nodes: 11, min_samples_leaf: 20|
| GradientBoostingRegressor | 14127       |   18841     | 0.92 | min_samples_split: 3, n_estimators: 300, subsample: 1.0|
| MLPRegressor      | 13600       | 18142       | 0.92       | mlp__hidden_layer_sizes: (10, 200, 10)       |

- a sample of important features (GradientBoostingRegressor), strangely ExterQual and Neighborhood are not as important as in KaggleSample, could not find my bug...

| feature   |  importance     |
|----------|----------|
| OverallQual | 0.49 |
| GrLivArea | 0.12 |
| TotalBsmtSF | 0.06 |
| GarageCars | 0.06 |
| 2ndFlrSF | 0.06 |
| BsmtFinSF1 | 0.03 |
| KitchenQual | 0.02 |
| 1stFlrSF | 0.02 |
| LotArea | 0.02 |
| ExterQual | 0.02 |

- for linear regression PCA was needed, for MLPRegressor not (worse results).

- raw results are in ~/git/houseprices/results/grid_train.txt

# discussion of best method

Eventhought MLP performed best in the 10% test set, we select GradientBoostingRegressor for submission.
This is because best MLP was not converged wth the given 1000 iterations.
When the calculation was repeated with MLP and best parameters with enought steps (10000),
results were worse compared to GradientBoostingRegressor. This may indicate, that model
starts overfitting, but was just luckily interrupted with 1000 iterations.

However, with dropouts and more thorough parameter search it is likely that MLP will be the best method studied here.

# clean and prepare test data

- usage for preparation for the test set:
```
cd ~/git/houseprices/houseprices/tasks
# TEST: prepare for random forest and other trees
python clean_data.py \
    --features_file ../../confs/houseprice_features.yaml \
    --instructions_file ../../confs/houseprice_test_forests.yaml

```

- resulting cleaned and normalised data (for the test set we do not have ground truth y)

```
~/git/houseprices/data/test_processed_X_for_tree.csv
```

# final submission with best method and best parameters (from the previous grid searches)

- run
```
cd ~/git/houseprices/houseprices/tasks
python submit_tree.py
```

- result in ~/git/houseprices/data/submission.csv

- looks roughly ok, the first five predictions are close to KaggleSample notebook results.

# Final discussion

- There may still be some bugs in the code. Most suspicious is that here features ExterQual and Neighborhood are not as important as in KaggleSample.

- MultiLayerPerceptor method could potentially give the best result, with more careful parameter tuning and dropouts to prevent overfittng.

- Greatest performance boost was achieved by changing when possible, categorical features to ordinal categorical features, that is, they have order from best to worst in some sense.

- Other three based methods could be tried from other libraries than scikit-learn.
